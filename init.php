<?php
//tell php where the libraries are when they're needed;
require_once 'vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;

// config the link to db for slim

    DB::$dbName = 'appleclone';
    DB::$password = 'vWjhqC5b3I2_O87O';
    DB::$user = 'appleclone';
    DB::$host = 'localhost';
    DB::$port = 3333;

 // Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails = true'
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1','VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};
                         