<?php
namespace App\Controllers;

class HomeController extends BaseController
{
    public function index($request, $response)
    {
        //use EloquentDB to retrieve db table users;
        //dd($this->container->get('eloquentDb')->table('users')->get());

        //use MeekroDb to retrive db
        //dd($this->container->get('MeekroDb')->query("select * from users"));

        return $this->container->view->render($response, 'home.twig');
    }
}