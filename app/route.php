<?php

/**
 * This is the place to register every route in this App
 * @var Slim\App $app
 */
$app->get('/first', function($request, $response) {
    return 'My first route';
});

$app->get('/home', function ($request, $response) {
    return $this->view->render($response, 'home.twig');
});

$app->get('/new', 'HomeController:index');
