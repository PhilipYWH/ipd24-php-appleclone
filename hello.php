<?php

require_once 'vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;


// Define app routes
$app->get('/hello/{name}', function ($request, $response, $args) {
    return $response->write("Hello " . $args['name']);
});

$app->get('/hello/{namePPP}/{agePPP:[0-9]+}', function ($request, $response, $args) {
    $name = $args['namePPP'];
    $age = $args['agePPP'];
    DB::insert('people', ['name' => $name, 'age' => $age]);
    $insertId = DB::insertId();
    return $this->view->render($response, 'hello.html.twig', ['ageTTT' => $age, 'nameTTT' => $name, 'insertIdTTT' => $insertId]);
    // return $response->write("<p>Hello $name, you are <b>$age</b> y/o</p>");
});