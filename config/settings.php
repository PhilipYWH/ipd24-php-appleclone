<?php
return [
    'settings' => [
        'addContentLengthHeader' => false,
        'displayErrorDetails' => true, // set to false in production
        // Renderer settings
        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'level' => Monolog\Logger::DEBUG,
            'path' => __DIR__ . '/logs/app.log',
        ],
        // Database connection settings
        "db" => [
            "driver" => "mysql",
            "host" => "localhost",
            "database" => "apple_clone",
            "username" => "root",
            "password" => "",
            "port" => "3333",
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],
    ],
];