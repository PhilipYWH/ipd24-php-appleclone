<?php

use Slim\App;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use App\Controllers\HomeController;
use Illuminate\Database\Capsule\Manager;

session_start();
require __DIR__ . '/../vendor/autoload.php';

//loading config/settings
$config = include '../config/settings.php';
$app = new App($config);

require __DIR__ . '/../app/route.php';

$container = $app->getContainer();


// Service factory for the ORM
$container['EloquentDb'] = function ($container) {
    $capsule = new Manager;
    $capsule->addConnection($container['settings']['db']);

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

// Service factory for the MeekroDb
$container['MeekroDb'] = function ($container) {
    $dbSettings = $container['settings']['db'];
    return new MeekroDB(
        $dbSettings['host'],
        $dbSettings['username'],
        $dbSettings['password'],
        $dbSettings['database'],
        $dbSettings['port'],
        $dbSettings['charset'],
    );
};

// Register Twig View helper
$container['view'] = function ($container) {
    $view = new Twig(__DIR__ . '/../resources/views', [
        'cache' => __DIR__ . '/../resources/views/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);

    //test purpose add a global variable for fun
    $view->getEnvironment()->addGlobal('test1','VALUE');

    $view->addExtension(new TwigExtension(
        $container->router,
        $container->request->getUri()
    ));
    return $view;
};

//Inject $container to HomeController
$container['HomeController'] = function ($container) {
    return new HomeController($container);
};
